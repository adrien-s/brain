#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <brain/tools/position3D.h>
#include <brain/tools/log.h>
#include <brain/neur/nGroup.h>

#include <test/test.h>

int testNeurBasic()
{
    /**
     * Creating variables we'll use
     */
    char str[255];
    neuron a;
    nGroup neurons;
    position3D ng_size;
    ng_size.x = ng_size.y = 500;
    ng_size.z = 100;

    /**
     * Initializing internal data to the default
     */
    /********** nr_init **********/
    logme(state_BUSY, "test : nr_init");
    nr_init(&a);
    if (a.count!=0 || a.weight!=NULL || a.connected!=NULL) {
        sprintf(str, "error : nr_init : count=%d weight=%p connected=%p", a.count, a.weight, a.connected);
        logme(state_KO, str);
        return 1;
    }
    logme(state_OK, "nr_init works.");

    /********** nr_reset **********/
    logme(state_BUSY, "test : nr_reset");
    if ((nr_reset(&a)!=0) || a.weight!=NULL || a.count!=0 || a.connected!=NULL) {
        sprintf(str, "error : nr_reset : count=%d weight=%p connected=%p", a.count, a.weight, a.connected);
        logme(state_KO, str);
        return 1;
    }
    logme(state_OK, "nr_reset works.");

    /********** ng_init ***********/
    logme(state_BUSY, "test : ng_init");
    if (ng_init(&neurons, &ng_size)!=0) {
        logme(state_KO, "error : ng_init");
        return 1;
    }
    logme(state_OK, "ng_init works.");

    /**
     * Deleting a neuron group
     */
    nGroup test;
    if (ng_init(&test, &ng_size)!=0) {
        return 1;
    }
    /********** ng_delete **********/
    logme(state_BUSY, "test : ng_delete");
    if (ng_delete(&test)!=0) {
        logme(state_KO, "error : ng_delete");
        return 1;
    }
    logme(state_OK, "ng_delete works.");

    /**
     * Adding two connections to neuron [0][0][0]
     */
    /********** nr_connect **********/
    logme(state_BUSY, "test : nr_connect");
    if ((nr_connect(neurons.cells[0][0][0], neurons.cells[1][2][3]) != 0) || (nr_connect(neurons.cells[0][0][0],neurons.cells[2][4][6])!=0) || (neurons.cells[0][0][0]->count!=2)) {
        logme(state_KO, "error : nr_connect");
        ng_delete(&neurons);
        return 1;
    }
    logme(state_OK, "nr_connect works.");

    /**
     * Testing neuron computing
     * ng_compute uses ng_compute_part which itself uses nr_compute on
     *  each neuron
     */
    /********** nr_compute **********/
    logme(state_BUSY, "test : nr_compute");
    if (nr_compute(neurons.cells[1][2][3]) != 0) {
        logme(state_KO, "error : nr_compute");
        ng_delete(&neurons);
        return 1;
    }

    /********** nr_compute_part **********/
    logme(state_BUSY, "test : ng_compute_part");
    position3D debSize, partSize;
    debSize.x = debSize.y = debSize.z = 5;
    partSize.x = partSize.y = partSize.z = 10;
    if (ng_compute_part(&neurons, &debSize, &partSize)!=0) {
        logme(state_KO, "error : ng_compute_part");
        ng_delete(&neurons);
        return 1;
    }

    /********** ng_compute **********/
    logme(state_BUSY, "test : ng_compute");
    if (ng_compute(&neurons)!=0) {
        logme(state_KO, "error : ng_compute");
        ng_delete(&neurons);
        return 1;
    }

    logme(state_OK, "compute functions work.");

    /**
     * Disconnecting previously created links
     */
    /********** nr_disconnect_ptr **********/
    logme(state_BUSY, "test : nr_disconnect_ptr");
    nr_disconnect_ptr(neurons.cells[0][0][0], neurons.cells[2][4][6]);
    if (neurons.cells[0][0][0]->count != 1) {
        logme(state_KO, "error : nr_disconnect_ptr");
        ng_delete(&neurons);
        return -1;
    }
    logme(state_OK, "nr_disconnect_ptr works.");

    /********** nr_disconnect **********/
    logme(state_BUSY, "test :  nr_disconnect");
    nr_disconnect(neurons.cells[0][0][0], 0); //Disconnect first link
    if (neurons.cells[0][0][0]->count!=0) {
        logme(state_KO, "error : nr_disconnect");
        ng_delete(&neurons);
        return 1;
    }
    logme(state_OK, "nr_disconnect works.");

    ng_delete(&neurons);

    /**
     * We've done with the tests !
     */
    return 0;
}
