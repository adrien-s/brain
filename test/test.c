#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <brain/tools/log.h>
#include <test/test.h>

int main(int argc, char ** argv)
{
    int ret;
    srand(time(NULL));

    logme(state_INFO, "Running [testNeurBasic] test…");
    ret = testNeurBasic();
    if (ret != 0) {
        logme(state_KO, "Test [testNeurBasic] failed !");
        return ret;
    }

    logme(state_INFO, "Running [testCDisplay] test…");
    ret = testCDisplay();
    if (ret != 0) {
        logme(state_KO, "Test [testCDisplay] failed !");
        return ret;
    }

    return ret;
}
