#ifndef _TEST_H_
#define _TEST_H_

/**
 * @brief testNeurBasic Basic neuron test
 * @return 0 : everything went well
 *        -1 : a test has failed
 */
int testNeurBasic();

/**
 * @brief testCDisplay control Display test
 * @return 0 : everything is ok
 *        -1 : an error has been caught
 */
int testCDisplay();

#endif
