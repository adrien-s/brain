#include <time.h>

#include <brain/tools/log.h>
#include <brain/neur/nGroup.h>
#include <brain/cDisplay.h>

#include <test/test.h>

int testCDisplay()
{
    /***** Creating variables *****/
    nGroup * neurons = malloc(sizeof(nGroup));
    position3D ng_size;
    cDisplay disp;
    ng_size.x = 160;
    ng_size.y = 50;
    ng_size.z = 20;
    u_int64_t i, j;

    logme(state_BUSY, "Initializing variables");
    if (ng_init(neurons, &ng_size)!=0) {
        logme(state_KO, "Couldn't create neuron group !");
        return 1;
    }
    if (cd_init(&disp)!=0) {
        logme(state_KO, "Coulnd't initiate cDisplay !");
        return 1;
    }

    disp.height = ng_size.y;
    disp.width = ng_size.x;
    disp.mvt_1.x = disp.mvt_2.y = 1;
    disp.origin.x = disp.origin.y = 0;
    disp.origin.z = 0;

    logme(state_OK, "Variables successfuly initialized");

    /***** Everything has been set up, we can start working *****/
    cd_clear();

    for(i=0 ; i<100 ; i++) {
        cd_walkNeurons(&disp, neurons);
        ng_compute(neurons);
        ng_randomConnect(neurons);
        for(j=0;j<5000;j++)
            neurons->cells[rand()%ng_size.z][rand()%ng_size.y][rand()%ng_size.x]->out = 5.f;
        disp.origin.z++;
        if (disp.origin.z>=ng_size.z)
            disp.origin.z=0;

    }
    cd_clear();
    logme(state_OK, "Done computing");

    /***** Done, we clean everything, now *****/

    logme(state_BUSY, "Freeing data");
    if (ng_delete(neurons)!=0) {
        logme(state_KO, "Coulnd't free neuron group !");
        return 1;
    }
    logme(state_OK, "Everything has been cleaned");

    return 0;
}
