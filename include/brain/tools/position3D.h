#ifndef _position3D_h_
#define _position3D_h_

/** Data */
typedef struct position3D {
	unsigned int x;
	unsigned int y;
	unsigned int z;
} position3D;

void p3d_init(position3D * p);
void p3d_add(position3D * p_1, const position3D * p_2);

#endif
