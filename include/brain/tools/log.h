#ifndef _log_h_
#define _log_h_

#define state_BUSY_str "[0m[[1;34mBUSY[0m]"
#define state_OK_str "[0m[ [1;32mOK[0m ]"
#define state_KO_str "[0m[[1;31mFAIL[0m]"
#define state_INFO_str "[0m[[1;33mINFO[0m]"

#define state_BUSY 0x4
#define state_OK 0x1
#define state_KO 0x2
#define state_INFO 0x8

void logme(unsigned int state, char * a);

#endif
