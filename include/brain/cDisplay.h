#ifndef _cDisplay_h_
#define _cDisplay_h_

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include <brain/neur/nGroup.h>
#include <brain/tools/position3D.h>

typedef struct cDisplay {
    position3D origin;
    int width;
    int height;

    position3D mvt_1;
    position3D mvt_2;
} cDisplay;

void cd_clear();
int cd_init(cDisplay * c);
void cd_walkNeurons(cDisplay * c, nGroup * ng);
void cd_showNeuronState(int x, int y, float value);

#endif
