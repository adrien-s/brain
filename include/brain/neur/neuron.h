#ifndef _neuron_h_
#define _neuron_h_

/** Data */
typedef struct neuron {
	float out;
	float * weight;
	struct neuron ** connected;
	unsigned int count;

	unsigned char modifiable;	
} neuron;

/**
 * Constants
 * forgetRatio : decrease speed of connection's weight when it's not used
 * reinforceRatio : increase speed of connection's weight when it's used
 * maxWeight : maximum weight for a connection
 * maxConnected : maximum number of connections within a same neuron
 * distanceMax : maximum length of a connection
 * dyingThreshold : when weight goes under that value, connection is deleted
 * threshold : minimum value of the calculated input to produce an output
 *   (below can be saw as a binay 0, above as a 1)
 */
#define nr_forgetRatio		0.9f
#define nr_reinforceRatio	1.5f
#define nr_maxWeight        20.f
#define nr_maxConnected		200
#define nr_distanceMax		5
#define nr_dyingThreshold	0.1f
#define nr_threshold		0.6f

/** Init neuron */
void nr_init(neuron * n);
int nr_reset(neuron * n);

/** Connection management */
int nr_connect(neuron * parent, neuron * connected);
int nr_disconnect_ptr(neuron * parent, neuron * connected);
int nr_disconnect(neuron * parent, unsigned int index);

/** Data management */
int nr_compute(neuron * cell);

#endif
