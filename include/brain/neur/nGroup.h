#ifndef _nGroup_h_
#define _nGroup_h_

#include <brain/tools/position3D.h>
#include <brain/neur/neuron.h>

// 3D neuron table
typedef struct nGroup {
	neuron **** cells;
	position3D size;
} nGroup;

/** Init nGroup */
int ng_compute_part(nGroup * ng, position3D * from, position3D * to);
int ng_compute(nGroup * ng);
int ng_init(nGroup * ng, position3D * size);
int ng_delete(nGroup * ng);

/** Utilities */
int ng_randomConnect(nGroup * ng);

#endif
