# Brain #

## Build ##
1. Create a directory somewhere in your filesystem
2. Run `cmake <path of project root dir> <parameters>` with parameters :  
	-DCMAKE_BUILD_TYPE=xxx  
		Debug : Build in debug mode  
		Release : Build in release mode  
	-DTESTSUITE=xxx   
		on : Build with test suite  
		off : Build normal application  
