/**
 * Neuron group
 *
 * Copyright © 2013, Art Software
 */

#include <stdio.h>
#include <stdlib.h>

#include <brain/neur/nGroup.h>

/**
 * Initializes a neuron group with a given size
 * (warning: will destroy any previous data!!)
 * @param ng The neuron group to init
 * @param size wanted size for group
 * @return 0: success, -1: invalid ptr
 */
int ng_init(nGroup * ng, position3D * size) {
	unsigned int i, j, k;
	if ( (ng == NULL) || (size == NULL) )
		return -1;
	ng->size = *size;

	ng->cells = (neuron ****) malloc(size->z * sizeof *(ng->cells));
	for (i=0 ; i<size->z ; i++) {
		ng->cells[i] = (neuron ***) malloc(size->y * sizeof *(ng->cells[i]));
		for (j=0 ; j<size->y ; j++) {
			ng->cells[i][j] = (neuron **) malloc (size->x * sizeof *(ng->cells[i][j]));
			for (k=0 ; k<size->x ; k++) {
				ng->cells[i][j][k] = (neuron *) malloc (sizeof *(ng->cells[i][j][k]));
				nr_init(ng->cells[i][j][k]);
			}
		}
	}

	return 0;
}

/**
 * Compute a part of a given neuron group
 * @param ng the neuron group to use for computation
 * @param from the start position to compute
 * @param to the end position to compute
 * @return 0: success, -1: invalid nGroup, -2: invalid coordinates
 */
int ng_compute_part(nGroup * ng, position3D * from, position3D * to) {
	if (ng == NULL)
		return -1;
	if ((from->x>to->x) || (from->y>to->y) || (from->z>to->z))
		return -2;

	// Computes each neuron of the group part
	unsigned int i, j, k;
	for(i=from->z;i<to->z;i++) {
		for(j=from->y;j<to->y;j++) {
			for(k=from->x;k<to->x;k++) {
				nr_compute(ng->cells[i][j][k]);
			}
		}
	}

	return 0;
}

/**
 * Compute an entire neuron group
 * @param ng the neuron group to use for computation
 * @return 0: success, -1: invalid nGroup, -2: invalid nGroup size
 */
int ng_compute(nGroup * ng) {
	if (ng == NULL)
		return -1;

	position3D orig;
	orig.x = orig.y = orig.z = 0;
	// Computes each neuron of the entire group
	return ng_compute_part(ng, &orig, &(ng->size));
}

/**
 * Delete a neuron group
 * @param ng The neuron group to delete
 * @return 0: Success, -1: invalid nGroup ptr
 */
int ng_delete(nGroup * ng) {
	unsigned int i, j, k;
	if (ng == NULL)
		return -1;

	if (ng->cells != NULL) {
		for (i=0 ; i<ng->size.z ; i++) {
			if (ng->cells[i] != NULL) {
				for (j=0 ; j<ng->size.y ; j++) {
					if (ng->cells[i][j] != NULL) {
						for (k=0 ; k<ng->size.x ; k++) {
							if (ng->cells[i][j][k] != NULL) {
								free(ng->cells[i][j][k]);
							}
						}
						free(ng->cells[i][j]);
					}
				}
				free(ng->cells[i]);
			}
		}
		free(ng->cells);
		ng->cells = NULL;
	}
	return 0;
}

/**
 * Creates a random connection between two neurons
 * @param ng The group to work on
 * @return 0: OK
 *        -1: Unable to allocate memory 
 *        -2: Cannot add connections: maximum reached
 *        -3: Invalid nGroup
 *        -4: Invalid nGroup size
 */
int ng_randomConnect(nGroup * ng) {
	if (ng == NULL) return -3;
	if ((ng->size.x == 0) || (ng->size.y == 0) || (ng->size.z == 0))
		return -4;

	neuron * parent, * child;
	child = NULL;
	
	parent = ng->cells[rand()%(ng->size.z)][rand()%(ng->size.y)][rand()%(ng->size.x)];
	while ((child == NULL) || (child==parent)) {
		child = ng->cells[rand()%(ng->size.z)][rand()%(ng->size.y)][rand()%(ng->size.x)];
	}

	return nr_connect(parent, child);
}
