/**
 * Basic neuron unit functions
 *
 * Copyright © 2013, Art Software
 */

#include <stdio.h>
#include <stdlib.h>

#include <brain/neur/neuron.h>
#include <brain/tools/log.h>

/**
 * init a neuron
 * @param n ptr to the neuron to init
 */
void nr_init(neuron * n) {
	n->weight = NULL;
	n->connected = NULL;
	n->count = 0;
	n->out = 0.f;
	n->modifiable = 1;
}

/**
 * reset a used neuron
 * @param n ptr to the neuron to reset
 * @return 0: OK, -1: invalid neuron ptr
 */
int nr_reset(neuron * n) {
	if (n == NULL) {
		return -1;
	}

	free(n->weight);
	free(n->connected);
	n->weight = NULL;
	n->connected = NULL;
	n->count = 0;
	n->out = 0.f;
	n->modifiable = 1;
	return 0;
}

/**
 * Add a connection
 * @param parent The neuron to modify
 * @param connected The neuron to add
 * @return 0: OK
 *        -1: unable to allocate memory
 *        -2: maximum connections number reached
 */
int nr_connect(neuron * parent, neuron * connected) {
	unsigned int i;
	neuron ** tmp = (neuron **) malloc((parent->count+1) * sizeof *tmp);
	float * tmp_w = (float *) malloc((parent->count+1) * sizeof *tmp_w);

	if ((tmp == NULL) || (tmp_w == NULL))
		return -1;

	if (parent->count == nr_maxConnected)
		return -2;

	// Add enought space in the arrays
	for(i=0 ; i<parent->count ; i++) {
		tmp[i] = parent->connected[i];
		tmp_w[i] = parent->weight[i];
	}

	tmp[parent->count] = connected;
    tmp_w[parent->count] = 1.f;

	// Replace original arrays with new ones
	free(parent->connected);
	free(parent->weight);
	parent->connected = tmp;
	parent->weight = tmp_w;

	parent->count++;
	return 0;
}

/**
 * Disconnect a previously connected neuron
 * @param parent the base neuron to act on
 * @param connected the neuron to disconnect
 * @return 0: OK, -1: parent ptr invalid, -2:No cell to disconnect, -3: neuron not found, -4: internal error 
 */
int nr_disconnect_ptr(neuron * parent, neuron * connected) {
	if (parent == NULL) return -1;
	if (parent->count == 0) return -2;
	unsigned char found = 0;
	unsigned int idx = 0;

	// Find the correct item
	while ((found == 0) && (idx<parent->count)) {
		if (parent->connected[idx] == connected)
			found = 1;
		else idx++;
	}
	if (found == 0) return -3;

	// Disconnect it
	if (nr_disconnect(parent, idx) != 0)
		return -4;

	return 0;
}

/**
 * Disconnect a previously connected neuron (with it's index num)
 * @param parent The base neuron to act on
 * @param index The number of the connected neuron to disconnect
 * @return 0: OK, -1: bad pointer, -2: invalid index
 */
int nr_disconnect(neuron * parent, unsigned int index) {
	if (parent == NULL) return -1;
	if (index>=parent->count) return -2;

	unsigned int i;
	neuron ** tmp = NULL;
	float * tmp_w = NULL;

	if (parent->count>1) {
		tmp = (neuron **) malloc((parent->count-1) * sizeof *tmp);
		tmp_w = (float *) malloc((parent->count-1) * sizeof *tmp_w);

		if ((tmp == NULL) || (tmp_w == NULL))
            return -1;

		// Paste before the deleted item
		for (i=0; i<index;i++) {
			tmp[i] = parent->connected[i];
			tmp_w[i] = parent->weight[i];
		}
		// Paste after the deleted item
        for (i=index+1 ; i<parent->count ; i++) {
			tmp[i-1] = parent->connected[i];
			tmp_w[i-1] = parent->weight[i];
		}
	}

	// Update items
	free(parent->connected);
	free(parent->weight);
	parent->connected = tmp;
	parent->weight = tmp_w;
	
	parent->count--;
	return 0;
}

/**
 * Computes a neuron's output using it's inputs
 * @param cell The neuron cell to use for computation
 * @return 0: OK, -1: bad cell pointer, -2: readonly cell
 */
int nr_compute(neuron * cell){
	if (cell == NULL) return -1;
	if (cell->modifiable == 0) return -2;

	// Computes the weighted average from neuron's outputs
	unsigned int i;
	float tmp, sumOutp=0.f, sumWeights=0.f;
	for (i=0 ; i<cell->count ; i++) {
        if (cell->connected[i]!=NULL)
        {
            tmp = (cell->connected[i]->out)*(cell->weight[i]);

            // if one weighted input is greater than the threshold,
            // reinforce the connection
            // else the connection will progressively be forgotten.
            if ( (tmp >= nr_threshold) && (cell->weight[i]<nr_maxWeight) )
                cell->weight[i] *= nr_reinforceRatio;
            else if (tmp < nr_threshold)
                cell->weight[i] *= nr_forgetRatio;

            sumOutp += tmp;
            sumWeights += cell->weight[i];

            // If a connection's weight goes below the minimum value,
            // it will be considered destroyed (and really disconnect it)
            if (cell->weight[i] < nr_dyingThreshold)
                cell->weight[i] = 0.f;
        }
    }

    // Remove dead connections
    i=0;
    while (i<cell->count) {
        if (cell->weight[i] == 0.f) {
            nr_disconnect(cell, i);
        } else {
            i++;
        }
    }

	tmp = 0.f;
	// Computes the average if there's at least one connection
	if (sumWeights>0)
		tmp = sumOutp/sumWeights;

	// Writes the result
	cell->out = tmp;
	return 0;
}

