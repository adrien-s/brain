#include <brain/cDisplay.h>

/**
 * Clears the screen.
 */
void cd_clear() {
    printf("[2J[1;1H");
}

/**
 * Initializes the struct.
 * @param c structure to initialize.
 * @return 0: success
 *        -1: invalid pointer
 */
int cd_init(cDisplay * c) {
	if (c == NULL) {
		return -1;
	}

    p3d_init(&(c->mvt_1));
    p3d_init(&(c->mvt_2));
    p3d_init(&(c->origin));
    c->width = c->height = 0;

    return 0;
}

/**
 * @brief cd_walkNeurons Displays neurons' state
 * @param c The config
 * @param ng The neuron group to look at
 */
void cd_walkNeurons(cDisplay * c, nGroup * ng)
{
    position3D p;
    int i, j;
    fprintf(stdout, "\e[1;1H");
    for(i=0; i<c->height; i++) {
        for (j=0; j<c->width; j++) {
            p.x = c->origin.x + (j * c->mvt_1.x) + (i * c->mvt_2.x);
            p.y = c->origin.y + (j * c->mvt_1.y) + (i * c->mvt_2.y);
            p.z = c->origin.z + (j * c->mvt_1.z) + (i * c->mvt_2.z);
            cd_showNeuronState(j, i, ng->cells[p.z][p.y][p.x]->out);
        }
    }
    fflush(stdout);
}

/**
 * @brief cd_showNeuronState Draws a neuron on the screen using ANSI Escape sequences
 * @param x the x position (0 <= x <= max)
 * @param y the y position (0 <= y <= max)
 * @param value the value to draw from
 */
void cd_showNeuronState(int x, int y, float value)
{
    int color=30; // 0 ↔ 1/4 : noir
    if (value*2.f > nr_threshold) // 1/4 ↔ 1/2 : gris foncé
        color=90;
    if (value > nr_threshold) // 1/2 ↔ 3/4 : gris clair
        color=37;
    if (value/1.5f > nr_threshold) // 3/4 ↔ 1 : blanc
        color=97;

    fprintf(stdout, "\e[%d;%dH\e[%dm*", 1+y, 1+x, color);
}
