/**
 * Logging function
 *
 * Copyright © 2013, Art Software
 */

#include <stdio.h>
#include <string.h>

#include <brain/tools/log.h>

/**
 * Logging events to console
 * @param state The current state
 * @param a The text to log
 */
void logme(unsigned int state, char * a) {
	char status[64];
	char end[2];

	sprintf(end, "\n");

	switch(state) {
		case 0:
            sprintf(status, "");
			break;
		case state_BUSY:
			sprintf(status, "%s ", state_BUSY_str);
			sprintf(end, "\r");
			break;
		case state_OK:
			sprintf(status, "%s ", state_OK_str);
			break;
        case state_INFO:
            sprintf(status, "%s ", state_INFO_str);
            break;
		default:
		case state_KO:
			sprintf(status, "%s ", state_KO_str);
			break;
	}
	printf("[2K%s%s%s", status, a, end);
	fflush(stdout);
}
