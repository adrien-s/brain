/**
 * Basic 3D positionning
 *
 * Copyright © 2013, Art Software
 */

#include <brain/tools/position3D.h>

/**
 * Init the position
 * @param p the position to init
 */
void p3d_init(position3D * p) {
	p->x = p->y = p->z = 0;
}

/**
 * Sums two positions together
 * @param p_1 The first parameter, which will be modified
 * @param p_2 The second parameter, will be summed to the other
 */
void p3d_add(position3D * p_1, const position3D * p_2) {
	p_1->x += p_2->x;
	p_1->y += p_2->y;
	p_1->z += p_2->z;
}
